<?php

use Acronis\Services\TaskBuilder;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

require __DIR__ . '/../vendor/autoload.php';

$app = AppFactory::create();
$app->addBodyParsingMiddleware();
$app->addRoutingMiddleware();
$app->addErrorMiddleware(true, true, true);


$app->post('/handle', function (Request $request, Response $response, array $args) {
    $data = $request->getParsedBody();
    $v = new Valitron\Validator($data);
    $v->rule('required', 'filename');
    $v->rule('required', 'tasks');
    $v->rule('required', 'tasks.*.name');
    $v->rule('required', 'tasks.*.command');

    if (!$v->validate()) {
        $response->getBody()->write(json_encode($v->errors()));
        $response->withHeader('content-type', 'application/json');
        $response->withStatus(422);

        return  $response;
    }
    $builder = new TaskBuilder($data['tasks']);
    $path = __DIR__ . "/../scripts/" . basename($data['filename']);
    $builder->write($path);
    $response->getBody()->write(json_encode([
        'message' => 'operation was successful',
        'code' => 200
    ]));
    return $response;
});

$app->run();