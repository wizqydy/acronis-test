## Getting Started
This is a simple application based on the slim microframework. In order to set up the application run the following.
```bash
git clone https://gitlab.com/wizqydy/acronis-test
cd acronis-test
#Install dependencies
composer install
#Run the php development server
php -S 0.0.0.0:8080 -t public
```

### Usage
After spinning up the PHP dev server, use curl or any other http client to send requests to the service.
```bash
curl -X POST http://localhost:8080/handle -H'Content-Type:application/json' --data-raw '
{
    "filename": "myscript.sh",
    "tasks": [
        {
            "name": "rm",
            "command": "rm -f /tmp/test",
            "dependencies": ["cat"]
        },
        {
            "name": "cat",
            "command": "cat /tmp/test",
            "dependencies": ["chown","chmod"]
        },
        {
            "name": "touch",
            "command": "touch /tmp/test"
        },
        {
            "name": "chmod",
            "command": "chmod 600 /tmp/test",
            "dependencies": ["touch"]
        },
        {
            "name": "chown",
            "command": "chown root:root /tmp/test",
            "dependencies": ["touch"]
        }
    ]
}'

```
If all goes well you will see the response below. and the script will be written to the file `scripts/myscript.sh`
```json
{"message":"operation was successful","code":200}
```

### Running Tests 
```bash
composer test
``` 