# Set the base image for subsequent instructions
FROM leitato/php-8.0:fpm-nginx

COPY . $APP_DIR

#Override global variables
ENV SUPERVISOR_USERNAME="admin" \
    SUPERVISOR_PASSWORD="secret"

RUN chown -R www-data:www-data $APP_DIR

WORKDIR $APP_DIR

RUN bash ${SCRIPTS_DIR}/composer.sh
