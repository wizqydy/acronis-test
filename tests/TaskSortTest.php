<?php

use Acronis\Services\TaskBuilder;

test('can sort tasks based on dependency', function () {
    $expected = file_get_contents(__DIR__ . '/data/expected.sh');

    $tasks = json_decode(file_get_contents(__DIR__ . '/data/tasks.json'), true);
    $builder = new TaskBuilder($tasks['tasks']);
    $commands = $builder->getCommands();
    expect(trim($commands))->toBe(trim($expected));
});

test('can write output to a file.', function () {
    $tasks = json_decode(file_get_contents(__DIR__ . '/data/tasks.json'), true);
    $builder = new TaskBuilder($tasks['tasks']);
    $path = __DIR__."/data/".basename($tasks['filename']);
    $builder->write($path);
    expect(file_exists($path))->toBeTrue();
    unlink($path);
});
