<?php

namespace Acronis;

class Task
{
    /**
     * The name of the task
     *
     * @var string
     */
    public string $name;

    /**
     * Command associated with the task
     *
     * @var string
     */
    public string $command;

    /**
     * Tasks that it depends on.
     *
     * @var array
     */
    public array $dependencies;

    /**
     * @param string $command
     */
    public function setCommand(string $command): void
    {
        $this->command = $command;
    }

    /**
     * @param array $dependencies
     */
    public function setDependencies(array $dependencies): void
    {
        $this->dependencies = $dependencies;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}