<?php

namespace Acronis\Services;

use Acronis\Task;

trait TopologySorter
{
    /**
     * Sort the tasks based on their dependencies and return the tasks in sorted.
     *
     * @param array $tasks
     * @return array
     *
     * @throws \Exception
     */
    public function sort(array $tasks): array
    {
        $sorted = [];
        $doneList = [];
        while (count($tasks) > count($sorted)) {
            foreach ($tasks as $task) {
                if (isset($doneList[$task->name])) {
                    continue;
                }
                $resolved = true;
                if (isset($task->dependencies)) {
                    foreach ($task->dependencies as $dep) {
                        if (!isset($doneList[$dep])) {
                            $resolved = false;
                            break;
                        }
                    }
                }
                if ($resolved) {
                    $doneList[$task->name] = true;
                    $sorted[] = $task;
                }
            }
        }
        return $sorted;
    }
}