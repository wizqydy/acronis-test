<?php

namespace Acronis\Services;

use Acronis\Task;

class TaskBuilder implements WriterInterface
{
    use TopologySorter;

    private array $tasks;

    public function __construct(array $tasks)
    {
        $this->setTasks($tasks);
    }


    /**
     * @return Task[]|array
     */
    public function getTasks(): array
    {
        return $this->tasks;
    }

    /**
     * Get the commands from the task list
     *
     * @return string
     * @throws \Exception
     */
    public function getCommands(): string
    {
        $commands = '';
        $tasks = $this->sort($this->tasks);
        foreach ($tasks as $task) {
            $commands .= $task->command . PHP_EOL;
        }

        return $commands;
    }

    /**
     * @param Task[]|array $taskList
     */
    private function setTasks(array $taskList): void
    {
        $this->tasks = array_map(function ($t) {
            $task = new Task();
            $task->setCommand($t['command']);
            $task->setName($t['name']);
            $task->setDependencies($t['dependencies'] ?? []);

            return $task;
        }, $taskList);
    }

    /**
     * Write the tasks to a file
     *
     * @param string $filename
     * @return void
     * @throws \Exception
     */
    public function write(string $filename)
    {
        if (!$this->ensureFileIsWritable($filename)) {
            throw new \RuntimeException(sprintf('the file %s is not writable', $filename));
        }
        file_put_contents($filename, $this->getCommands());
    }

    protected function ensureFileIsWritable($filename): bool
    {
        return file_exists($filename) && is_writable($filename) || touch($filename);
    }
}