<?php

namespace Acronis\Services;

interface WriterInterface
{
    public function write(string $filename);
}